# layui app exp 单 页 面 应 用 拓 展

> 🎏 layui 单 页 面 应 用 拓 展

layui框架:[https://layui.dev/](https://layui.dev/)

appjs拓展:[http://layui.app.kllxs.top/](http://layui.app.kllxs.top/)

## 使用

index.html

```html
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Layui单页面应用</title>
    <!-- layui -->
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.js"></script>
    <script>
        // 配置
        layui.config({
            base: "model/", // 模块路径
        }).use(["app"], function () {
            let app = layui.app

            app.init({
                main: "main", //入口标签
                component: "/component",// 组件
                middleware: [ // 中间件
                    "/middleware/test.js"
                ],
                route: "/route.json" // 路由；url/obj
            })

        })
    </script>
</head>

<body>
    <!-- 入口标签 -->
    <main></main>
</body>

</html>
```

## 基础

### 目录结构

```
www web部署目录
├─component     组件目录
│
├─layui         layui源码
│
├─middleware    中间件目录
│
├─model         layui模块目录
│  ├─ ...
│  └─app.js     app拓展源码文件
│
├─page          路由文件目录
│
├─.gitignore    git规则
├─index.html    入口文件
├─LICENSE.txt   授权说明
├─README.md     自述文件
└─route.json    路由文件
```

### 配置

```js
// 配置
layui.config({
    base: "model/", // 模块目录路径 app.js
}).use(["app"], function () {
    // app 
    let app = layui.app
    // 初始化
    app.init({
        main: "main", //入口标签
        component: "/component",// 组件
        middleware: [ // 中间件
            "/middleware/test.js"
        ],
        route: "/route.json" // 路由；url/obj
    })

})
```

## 路由

```json
{
    "home": { // 路由名 你可以认为是 id 名
        "page": "/page/home.html", //路由页面
        "path": [ // 匹配
            "/",
            "/index",
            "/index.html"
        ]
    },
    "about": {
        "page": "/page/about.html",
        "path": [
            "/about",
            "/about.html"
        ]
    },
    "order": {
        "page": "/page/order.html",
        "path": [
            "/order/{id}" //参数匹配 id ;如：/order/12, {id:"12"}
        ]
    },
    "error": { // error路由名为错误路由，可以不填
        "page": "/page/404.html", // 错误页面
        "path": [] // 为空数组即可
    }
    // 以此类推
}
```

### 页面模板

```html
<template>
    <!-- 必须template标签包裹的任意内容 -->
    <script>
        
        console.log(shadow, route, onDestroy)

        // 声明周期->销毁函数
        onDestroy(function(){
            console.log("我已经销毁了")
        })

    </script>
</template>
```

#### 模板内置方法

| 方法 | 类型 | 说明 |
| ---- | ---- | ---- |
| shadow | DOM | 当前页面的影子dom对象 |
| route | obj | 当前页面的请求信息参数 |
| onDestroy | function | 生命周期;页面卸载函数 | 

## 组件

- 使用方法`layui.app.component(select,DOM = document, path = "")`

| 参数 | 说明 | 类型 | 默认 |
| ---- | ---- | ---- | ---- |
| select | css 选择器| string | 必需 |
| DOM | dom的位置，在组件内可以填入shadow | dom | document|
| path | 组件位置<br>配置中设置了组件文件会进行拼接<br>"/component/"+path+".html" ,不填则path=select | string | "" |

### 模板

> 属性改变模板自动刷新，不建议组件内加定时器

```html
<!-- 必须template标签包裹的任意内容 -->
<template>
    hello world
    <!-- 支持原生插槽 -->
    <slot name="my-slot">is slot</slot>
    <script>
        console.log(DOM,shadow,attrs)
    </script>
</template>
```

#### 模板内置方法

| 方法 | 类型 | 说明 |
| ---- | ---- | ---- |
| DOM | DOM | 当前组件dom对象 |
| shadow | DOM | 当前组件的影子dom对象 |
| attrs | obj | 当前组件的属性对象 |

## 中间件

- 使用方法`middleware.use(callback)`

```js
middleware.use((input) => {
    // 请求的路由信息
    console.log(input)
    // 返回true则通过否则不通过
    return true
})
```

## jQuery 的使用

```html
<template>
    
    <p id="my-p"> jQuery获取P的 </p>
    
    <script>
        layui.use("jquery",function(){
            let $ = layui.$

            // jQuery获取p标签，shadow是当前模板的 影子 dom 
            let my_p = $("#my-p",shadow)
            console.log(my_p)
        })
    </script>
</template>
```

# 暂时就这么点...